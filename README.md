# cpong

Classic PONG remake written in C using SDL2.

How to build (requires `SDL2` and `SDL2_image`):

```sh
mkdir -p bin
gcc src/*.c $(sdl2-config --cflags --libs) -lSDL2_image -Wall -o bin/game
```
