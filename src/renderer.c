#include "renderer.h"
#include "SDL_image.h"
#include "SDL_render.h"
#include "types.h"
#include <stdbool.h>

bool init() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) == 0) {
        window = SDL_CreateWindow("Pong", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowSize.x, windowSize.y,
                                  SDL_WINDOW_RESIZABLE);
        if (window != NULL) {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (renderer != NULL) {
                SDL_RenderSetLogicalSize(renderer, logicalSize.x, logicalSize.y);
                int flags = IMG_INIT_PNG;
                if ((IMG_Init(flags) & flags) != flags) {
                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                                 "Failed to init required jpg and png support! Error: %s\n", IMG_GetError());
                } else {
                    SDL_Surface *tmp;
                    tmp = IMG_Load("img/cpong_start.png");
                    textures[0] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_end1.png");
                    textures[1] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_end2.png");
                    textures[2] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score0.png");
                    textures[3] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score1.png");
                    textures[4] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score2.png");
                    textures[5] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score3.png");
                    textures[6] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score4.png");
                    textures[7] = SDL_CreateTextureFromSurface(renderer, tmp);
                    tmp = IMG_Load("img/cpong_score5.png");
                    textures[8] = SDL_CreateTextureFromSurface(renderer, tmp);
                    return false;
                }
            } else {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create renderer. Error: %s\n", SDL_GetError());
                SDL_DestroyWindow(window);
            }
        } else {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create window. Error: %s\n", SDL_GetError());
        }
    } else {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize SDL. Error: %s\n", SDL_GetError());
    }

    return true;
}

void presentStartScene() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, textures[0], NULL, NULL);
    SDL_RenderPresent(renderer);
}

void presentGameScene(SDL_FRect ball, SDL_FRect leftPaddle, SDL_FRect rightPaddle) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    SDL_Rect gameArea;
    gameArea.x = 0;
    gameArea.y = 0;
    gameArea.w = logicalSize.x;
    gameArea.h = logicalSize.y;
    SDL_SetRenderDrawColor(renderer, 30, 30, 30, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(renderer, &gameArea);

    SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRectF(renderer, &ball);
    SDL_RenderFillRectF(renderer, &leftPaddle);
    SDL_RenderFillRectF(renderer, &rightPaddle);

    SDL_RenderPresent(renderer);
}

void presentGoalScene(SDL_FRect ball, SDL_FRect leftPaddle, SDL_FRect rightPaddle, int left, int right) {
    SDL_Delay(400);
    for (int i = 0; i < 10; i++) {
        int redColor = (i % 2 == 0) ? 255 : 0;
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        SDL_Rect gameArea;
        gameArea.x = 0;
        gameArea.y = 0;
        gameArea.w = logicalSize.x;
        gameArea.h = logicalSize.y;
        SDL_SetRenderDrawColor(renderer, 30, 30, 30, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRect(renderer, &gameArea);

        SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRectF(renderer, &leftPaddle);
        SDL_RenderFillRectF(renderer, &rightPaddle);
        SDL_SetRenderDrawColor(renderer, redColor, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRectF(renderer, &ball);

        SDL_Rect leftScore;
        leftScore.w = 0.2 * logicalSize.x;
        leftScore.h = 0.2 * logicalSize.x;
        leftScore.x = 0.2 * logicalSize.x - 1;
        leftScore.y = (logicalSize.y - 1) / 2 - leftScore.h / 2;
        SDL_SetRenderDrawColor(renderer, 0, 0, 200, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRect(renderer, &leftScore);
        SDL_RenderCopy(renderer, textures[3 + left], NULL, &leftScore);

        SDL_Rect rightScore;
        rightScore.w = 0.2 * logicalSize.x;
        rightScore.h = 0.2 * logicalSize.x;
        rightScore.x = logicalSize.x * 0.8 - leftScore.w;
        rightScore.y = (logicalSize.y - 1) / 2 - leftScore.h / 2;
        SDL_SetRenderDrawColor(renderer, 0, 0, 200, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRect(renderer, &rightScore);
        SDL_RenderCopy(renderer, textures[3 + right], NULL, &rightScore);
        SDL_RenderPresent(renderer);

        SDL_Delay(300);
    }
}

void presentEndScene(int winner) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, textures[winner], NULL, NULL);
    SDL_RenderPresent(renderer);
}

void shutdown() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
}
