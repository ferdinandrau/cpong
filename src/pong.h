#pragma once

#include <stdint.h>

#include "renderer.h"
#include "types.h"

player p1;
player p2;
ball b;

GameMode mode;
GameState state;

uint64_t lastFrame;
