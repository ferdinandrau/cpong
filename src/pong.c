#include <math.h>

#include "pong.h"
#include "types.h"

#define INITIAL_WINDOW_WIDTH 1080
#define INITIAL_WINDOW_HEIGHT 720
#define RENDERER_LOGICAL_WIDTH 960
#define RENDERER_LOGICAL_HEIGTH 720

bool coinToss() { return rand() > (RAND_MAX / 2); }

void setupBoard() {
    b.rect.h = RENDERER_LOGICAL_HEIGTH * 0.02;
    b.rect.w = RENDERER_LOGICAL_HEIGTH * 0.02;
    b.rect.x = (RENDERER_LOGICAL_WIDTH - b.rect.w) / 2;
    b.rect.y = (RENDERER_LOGICAL_HEIGTH - b.rect.h) / 2;

    b.velocity.x = 0.35 * RENDERER_LOGICAL_WIDTH;
    if (coinToss())
        b.velocity.x = -b.velocity.x;
    b.velocity.y = 0.4 * RENDERER_LOGICAL_WIDTH;
    if (coinToss())
        b.velocity.y = -b.velocity.y;

    SDL_FRect paddle1 = {0, logicalSize.y * 0.75 / 2 - 1, logicalSize.x * 0.02, logicalSize.y * 0.25};
    p1.rect = paddle1;
    SDL_FRect paddle2 = {logicalSize.x - logicalSize.x * 0.02, logicalSize.y * 0.75 / 2 - 1, logicalSize.x * 0.02,
                         logicalSize.y * 0.25};
    p2.rect = paddle2;

    SDL_Log("Board set to default state.\n");
}

void movePaddles(double dt) {
    const double PADDLE_VELOCITY = 0.35 * logicalSize.y;
    const double PADDLE_MOVE_PIXELS = PADDLE_VELOCITY * dt;

    if (p1.up && !p1.down) {
        if (p1.rect.y >= PADDLE_MOVE_PIXELS)
            p1.rect.y -= PADDLE_MOVE_PIXELS;
        else
            p1.rect.y = 0;
    } else if (!p1.up && p1.down) {
        if (p1.rect.y + p1.rect.h + PADDLE_MOVE_PIXELS <= logicalSize.y)
            p1.rect.y += PADDLE_MOVE_PIXELS;
        else
            p1.rect.y = logicalSize.y - p1.rect.h;
    }
    if (p2.up && !p2.down) {
        if (p2.rect.y >= PADDLE_MOVE_PIXELS)
            p2.rect.y -= PADDLE_MOVE_PIXELS;
        else
            p2.rect.y = 0;
    } else if (!p2.up && p2.down) {
        if (p2.rect.y + p2.rect.h + PADDLE_MOVE_PIXELS <= logicalSize.y)
            p2.rect.y += PADDLE_MOVE_PIXELS;
        else
            p2.rect.y = logicalSize.y - p2.rect.h;
    }
}

void doComputerMove() {
    // TODO: implementation
}

double2d handleWallBounce(ball b, double dt) {
    double2d result = {b.rect.y + b.velocity.y * dt, b.velocity.y};

    while (result.x < 0.0f || result.x + b.rect.h >= (double)logicalSize.y) {
        if (result.x < 0.0f) {
            result.x = -result.x;
        } else if (result.x + b.rect.h >= (double)logicalSize.y) {
            result.x = logicalSize.y - (result.x + b.rect.h - logicalSize.y) - b.rect.h;
        }
        result.y = -result.y;
    }

    return result; // .x = Y-position, .y = Y-velocity
}

void moveBall(double dt) {
    double2d currentBallPos, nextBallPos;
    currentBallPos.x = b.rect.x;
    currentBallPos.y = b.rect.y;
    nextBallPos.x = b.rect.x + b.velocity.x * dt;
    nextBallPos.y = b.rect.y + b.velocity.y * dt;

    double2d yResult = handleWallBounce(b, dt);
    nextBallPos.y = yResult.x;
    b.velocity.y = yResult.y;

    double moveWidth = fabs(currentBallPos.x - nextBallPos.x);
    double ratio;
    double2d impactPosition;

    while (nextBallPos.x <= p1.rect.w || nextBallPos.x + b.rect.w - 1 >= p2.rect.x) {
        if (nextBallPos.x <= p1.rect.w) {
            ratio = (currentBallPos.x - p1.rect.w) / moveWidth;
            impactPosition.x = currentBallPos.x - moveWidth * ratio;
            impactPosition.y = handleWallBounce(b, ratio * dt).x;
            if (currentBallPos.x >= p1.rect.w && ratio >= 0 && impactPosition.y <= p1.rect.y + p1.rect.h &&
                impactPosition.y + b.rect.h >= p1.rect.y) {
                b.velocity.x = -b.velocity.x;
                nextBallPos.x = impactPosition.x + fabs(currentBallPos.x - impactPosition.x);
            } else {
                if (nextBallPos.x <= 0) {
                    ratio = fabs(nextBallPos.x) / moveWidth;
                    nextBallPos.y = handleWallBounce(b, ratio * dt).x;
                    nextBallPos.x = 0;
                }
                break;
            }
        } else if (nextBallPos.x + b.rect.w - 1 >= p2.rect.x) {
            ratio = (p2.rect.x - (currentBallPos.x + b.rect.w - 1)) / moveWidth;
            impactPosition.x = currentBallPos.x + moveWidth * ratio;
            impactPosition.y = handleWallBounce(b, ratio * dt).x;
            if (currentBallPos.x + b.rect.w - 1 < p2.rect.x && ratio >= 0 &&
                impactPosition.y <= p2.rect.y + p2.rect.h && impactPosition.y + b.rect.h >= p2.rect.y) {
                b.velocity.x = -b.velocity.x;
                nextBallPos.x = impactPosition.x - fabs(currentBallPos.x - impactPosition.x);
            } else {
                if (nextBallPos.x + b.rect.w >= logicalSize.x) {
                    ratio = (logicalSize.x - 1 - (currentBallPos.x + b.rect.w)) / moveWidth;
                    nextBallPos.y = handleWallBounce(b, ratio * dt).x;
                    nextBallPos.x = logicalSize.x - b.rect.w;
                }
                break;
            }
        }
    }

    b.rect.x = nextBallPos.x;
    b.rect.y = nextBallPos.y;
}

void update(bool *quit) {
    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
        switch (ev.type) {
            case SDL_QUIT:
                SDL_Log("Quit requested, finishing the frame and exiting...\n");
                *quit = true;
                break;
            case SDL_WINDOWEVENT:
                switch (ev.window.event) {
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                        SDL_Log("Window %d resized to %dx%d", ev.window.windowID, ev.window.data1, ev.window.data2);
                        windowSize.x = ev.window.data1;
                        windowSize.y = ev.window.data2;
                }
                break;
            case SDL_KEYUP:
                switch (ev.key.keysym.sym) {
                    case SDLK_w:
                        p1.up = false;
                        break;
                    case SDLK_s:
                        p1.down = false;
                        break;
                    case SDLK_UP:
                        if (mode == TWO_PLAYERS)
                            p2.up = false;
                        break;
                    case SDLK_DOWN:
                        if (mode == TWO_PLAYERS)
                            p2.down = false;
                }
                break;
            case SDL_KEYDOWN:
                if (!ev.key.repeat) {
                    switch (ev.key.keysym.sym) {
                        case SDLK_w:
                            p1.up = true;
                            break;
                        case SDLK_s:
                            p1.down = true;
                            break;
                        case SDLK_UP:
                            if (mode == TWO_PLAYERS)
                                p2.up = true;
                            break;
                        case SDLK_DOWN:
                            if (mode == TWO_PLAYERS)
                                p2.down = true;
                            break;
                        case SDLK_a:
                            if (state == START)
                                mode = TWO_PLAYERS;
                        case SDLK_b:
                            if (state == START) {
                                if (ev.key.keysym.sym != SDLK_a)
                                    mode = SINGLE_PLAYER;
                                setupBoard();
                                state = RUNNING;
                                lastFrame = SDL_GetPerformanceCounter();
                            }
                        case SDLK_r:
                            if (state == END) {
                                p1.score = 0;
                                p2.score = 0;
                                setupBoard();
                                state = START;
                            }
                            break;
                        case SDLK_p:
                            if (state == RUNNING) {
                                state = PAUSED;
                            } else if (state == PAUSED) {
                                state = RUNNING;
                                lastFrame = SDL_GetPerformanceCounter();
                            }
                            break;
                        case SDLK_q:
                            if (state == START || state == PAUSED || state == END) {
                                SDL_Log("Quit requested, finishing the frame and exiting...\n");
                                *quit = true;
                            }
                            break;
                    }
                }
        }
    }

    if (state == POINT) {
        if (p1.score == 5 || p2.score == 5) {
            state = END;
            if (p1.score == 5)
                SDL_Log("Player 1 wins!\n");
            if (p2.score == 5)
                SDL_Log("Player 2 wins!\n");
        } else {
            state = DELAY;
        }
        setupBoard();
    } else if (state == DELAY) {
        SDL_Delay(1000);
        state = RUNNING;
        lastFrame = SDL_GetPerformanceCounter();
    }

    if (b.rect.x <= 0 || b.rect.x + b.rect.w >= logicalSize.x) {
        state = POINT;
        if (b.rect.x <= 0)
            SDL_Log("Player 2 has scored! New score: %d\n", ++p2.score);
        else
            SDL_Log("Player 1 has scored! New score: %d\n", ++p1.score);
    }

    if (state != RUNNING)
        return;

    uint64_t currentFrame = SDL_GetPerformanceCounter();
    double dt = (double)(currentFrame - lastFrame) / SDL_GetPerformanceFrequency();
    lastFrame = currentFrame;

    if (mode == SINGLE_PLAYER)
        doComputerMove();

    movePaddles(dt);
    moveBall(dt);
}

void draw() {
    switch (state) {
        case START:
            presentStartScene();
            break;
        case DELAY:
        case RUNNING:
        case PAUSED:
            presentGameScene(b.rect, p1.rect, p2.rect);
            break;
        case POINT:
            presentGoalScene(b.rect, p1.rect, p2.rect, p1.score, p2.score);
            break;
        case END:
            if (p1.score == 5)
                presentEndScene(1);
            else
                presentEndScene(2);
    }
}

int main(int argc, char *args[]) {
    windowSize.x = INITIAL_WINDOW_WIDTH;
    windowSize.y = INITIAL_WINDOW_HEIGHT;
    logicalSize.x = RENDERER_LOGICAL_WIDTH;
    logicalSize.y = RENDERER_LOGICAL_HEIGTH;

    if (init())
        return 1;

    p1.score = 0;
    p2.score = 0;
    setupBoard();
    state = START;

    bool quit = false;
    while (!quit) {
        update(&quit);
        draw();
    }

    shutdown();
    return 0;
}
