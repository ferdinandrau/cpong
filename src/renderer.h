#pragma once

#include "types.h"
#include <SDL.h>

int2d windowSize;
int2d logicalSize;

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *textures[9];

bool init();
void presentStartScene();
void presentGameScene(SDL_FRect ball, SDL_FRect leftPaddle, SDL_FRect rightPaddle);
void presentGoalScene(SDL_FRect ball, SDL_FRect leftPaddle, SDL_FRect rightPaddle, int left, int right);
void presentEndScene(int winner);
void shutdown();
