#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <stdbool.h>

typedef struct {
    int x;
    int y;
} int2d;

typedef struct {
    double x;
    double y;
} double2d;

typedef struct {
    bool cpu;
    int score;
    bool up;
    bool down;
    SDL_FRect rect;
} player;

typedef struct {
    double2d velocity;
    SDL_FRect rect;
} ball;

typedef enum { TWO_PLAYERS, SINGLE_PLAYER } GameMode;
typedef enum { START, RUNNING, PAUSED, DELAY, POINT, END } GameState;
